// The function to differenciate
function y=F(x)
  y=[sin(x + (%pi/4)) - 10^(-x)];
endfunction
x = [1:0.1:2]
t = numderivative(F, x')

//10^(-x)*ln(10)+cos(x + (%pi/4))          first derivative
function y_ = perv(x)
    y_ = 10^(-x)*log(10)+cos(x + (%pi/4))
endfunction
//-10^(-x)*(ln(10))^2-sin(x + (%pi/4))     second derivative
function s = vtor(x)
    s = -10^(-x)*(log(10))^2-sin(x + (%pi/4))
endfunction

a = 2
b = 3
x_n = 2.99
eps = 0.00001
dx = 0
fa = 0
fb = 0

//while (abs(F(x_n)/perv(x_n)) > eps)
//    x_n = x_n - (F(x_n)/perv(x_n))
//end

//mputl(string(x_n), fd)


//disp(x_n)
//fd=mopen('F:\FCS_Sokolov_2019_A\PoAP\Lab3\result1.txt','wb')
//mputl(string(x_n), fd)
//save(fd, x_n)
//mclose(fd)

fd=mopen('F:\FCS_Sokolov_2019_A\PoAP\labs\Lab3\result1.txt','a+b')

mputl('====a==========b============dx===========f(a)==========f(b========)', fd);
//print(fd,"====a======b====dx=========f(a)=======f(b)")
mputl(ascii(10) + ascii(13), fd)

function print_to_file()
    disp(a)
    disp(b)
    
     output = (string(a) + "   " + string(b) + "    " + string(dx) + "   "+ string(fa) + "   "+ string(fb));
    //output = (output + string(dx)) + "   "
    //output = (output + string(fa)) + "   "
    //output = output + string(fb)
        mputl(output, fd);
        mputl(ascii(10) + ascii(13), fd)
        
    
endfunction


if ((F(b) * vtor(x_n)) >=0 ) then
    while (abs (b-a) > eps)
        b_1 = b - (F(b)/ perv(b))
        a_1 = a - ((b - a)*F(a))/(F(b) - F(a))
        a = a_1
        b = b_1
    end
end


if ((F(b) * vtor(x_n)) < 0 ) then
    while (abs (b-a) > eps)
        a_1 = a - (F(a)/ perv(a))
        b_1 = b -((b - a)*F(b))/(F(b) - F(a));
        a = a_1
        b = b_1
        dx = abs(a - b)
        fa = F(a)
        fb = F(b)
        print_to_file()
        
       
        
    end
end
mclose(fd)

