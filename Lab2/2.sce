f = figure()

dbt=uicontrol(f,'Style','pushbutton','position',[10,75,100,20])
set(dbt,'String','нарисовать', 'Callback', 'drawgraph')

set(f, 'figure_name', 'окошечко')
l_text = uicontrol(f,'style','edit', ...
 'position', [10 10 200 20])
set(l_text, 'value' , 2)
r_text = uicontrol(f,'style','edit', ...
 'position', [10 40 200 20])
 set(r_text, 'value' , 3)

//textresult=uicontrol(f, 'style', 'text', 'string','','position', [15, 80, 650, 200]);
function drawgraph()
    lt = get(l_text, 'string');
    rt = get(r_text, 'string');
    ym_ = evstr(lt);
    xm_ = evstr(rt);
    //set(textresult, 'string', sprintf(xm_))
    clf();    //2 ... 3
    x=[xm_:0.01:ym_]
    plot2d(x, sin(x + (%pi/4)) - 10^(-x), axesflag=5, rect = [xm_,-1,ym_,1])
endfunction

