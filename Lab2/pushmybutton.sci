function pushmybutton ( huibutton )
  disp("Push!")
endfunction

hmain = scf();
h = figure(hmain);
h.Position = [0 0 200 20];
huibutton=uicontrol(h,"style","pushbutton");
huibutton.Position = [0 0 200 20];
huibutton.String = "Update";
huibutton.BackgroundColor=[0.9 0.9 0.9];
huibutton.Callback = "pushmybutton";
