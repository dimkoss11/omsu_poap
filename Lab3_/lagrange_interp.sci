function y = lagrange_interp(x,data)
    
    for i = 1:length(x)
        y(i) = P(x(i),data);
    end
    
endfunction


function y = P(x,data)

    n = size(data,1);

    xi = data(:,1);
    yi = data(:,2);

    L = cprod_e(xi,x) ./ cprod_i(xi); 
    y = yi' * L;

endfunction

function y = cprod_e(x,a)
    
    n = length(x);
    y(1) = prod(a-x(2:$));
    for i = 2:n-1
        y(i) = prod(a-x(1:i-1))*prod(a-x(i+1:$));
    end
    y(n) = prod(a-x(1:$-1));
    
endfunction


function y=cprod_i(x)
    
    n = length(x);
    y(1) = prod(x(1)-x(2:$));
    for i = 2:n-1
        y(i) = prod(x(i)-x(1:i-1))*prod(x(i)-x(i+1:$));
    end
    y(n) = prod(x(n)-x(1:$-1));
    
endfunction


data1=[...
-1.0 0.038;
-0.8 0.058;
-0.6 0.1;
-0.4 0.2;
-0.2 0.5;
0.0 1.0;
0.2 0.5;
0.4 0.2;
0.6 0.1;
0.8 0.058;
1.0 0.038];



x = linspace(-1,1,1000);
y = lagrange_interp(x,data1);

plot(x,y,'-b',...
x, 1.0 ./ (1+25.*x.^2),'--g',...
data1(:,1),data1(:,2),'or')


data2 = [...
-1.0 0.038;
-0.95 0.042;
-0.81 0.058;
-0.59 0.104;
-0.31 0.295;
0.0 1.0;
0.31 0.295;
0.59 0.104;
0.81 0.058;
0.95 0.042;
1.0 0.038];


y = lagrange_interp(x,data2);

plot(x,y,'-b',...
x, 1.0 ./ (1+25.*x.^2),'--g',...
data2(:,1),data2(:,2),'or')


