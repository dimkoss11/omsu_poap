function symboliclagrange(X,Y)
    x = poly(0,"x")
    for i=1:length(Y)
        l(i)=1
        for j=1:length(Y)
            if i~=j
                l(i) = l(i)*(x-X(j))/(X(i)-X(j))
            end
        end
    end
    L=0
    for i=1:length(Y)
        L = L+Y(i)*l(i)
    end
    disp(L)
endfunction

X = [0 1 5 8]
Y = [0 1 8 16.4]

//symboliclagrange(X, Y)

x_ = [0.0 0.122173 0.2443461 0.3665191]
y_ = [1.0 0.9925462 0.9702957 0.9335804]

symboliclagrange(x_, y_)
