\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Нахождение корней линейного уравнения методом обратной матрицы}{2}% 
\contentsline {section}{\numberline {1.1}Теория}{2}% 
\contentsline {section}{\numberline {1.2}Исходный код}{2}% 
\contentsline {section}{\numberline {1.3}Результат работы}{3}% 
\contentsline {chapter}{\numberline {2}Нахождение корня методом касательных}{4}% 
\contentsline {section}{\numberline {2.1}Теория}{4}% 
\contentsline {section}{\numberline {2.2}Исходный код}{4}% 
\contentsline {section}{\numberline {2.3}Результат работы}{6}% 
\contentsline {chapter}{\numberline {3}Полином Лагранжа}{7}% 
\contentsline {section}{\numberline {3.1}Теория}{7}% 
\contentsline {section}{\numberline {3.2}Исходный код}{7}% 
\contentsline {section}{\numberline {3.3}Результат работы}{8}% 
